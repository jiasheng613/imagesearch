const DB = require("../data/db");

const PersonModel = DB.model("person", {
    name: String,
    email: String,
    password: String
});

module.exports = PersonModel;