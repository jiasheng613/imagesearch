const {
    GraphQLID,
    GraphQLString,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLSchema
} = require("graphql");

const bcrypt = require('bcryptjs');
const jsonwebtoken = require('jsonwebtoken');
require('dotenv').config();

const PersonModel = require("../model/person");

const { search_images } = require('../services');

const PersonType = new GraphQLObjectType({
    name: "Person",
    fields: {
        id: { type: GraphQLID },
        email: { type: GraphQLString },
        name: { type: GraphQLString },
        password: { type: GraphQLString }
    }
});

const AuthPayloadType = new GraphQLObjectType({
    name: "AuthPayload",
    fields: {
        user: { type:PersonType },
        token: { type: GraphQLString }
    }
});

const SearchImageType = new GraphQLObjectType({
    name: "SearchImage",
    fields: {
        image_ID: { type: GraphQLString },
        thumbnails: { type: GraphQLString },
        preview: { type: GraphQLString },
        title: { type: GraphQLString },
        source: { type: GraphQLString },
        tags: { type: GraphQLList(GraphQLString) },
    }
});

const schema = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: "Query",
        fields: {
            me:{
                type: PersonType,
                resolve: async(root, args, { user }, info) => {
                    if (!user) {
                        throw new Error('Please login!');
                    }
                    const { id } = user;
                    const suser = await PersonModel.findOne({_id:id}).exec();
                    if(!suser){
                        throw new Error('No such user found!');
                    }
                    return suser;
                }
            },
            users: {
                type: GraphQLList(PersonType),
                resolve: (root, args, { user }, info) => {
                    if(!user){
                        throw new Error('Please login!!');
                    }
                    return PersonModel.find().exec();
                }
            },
            user: {
                type: PersonType,
                args: {
                    id: { type: GraphQLNonNull(GraphQLID) }
                },
                resolve: (root, { id }, { user }, info) => {
                    if(!user){
                        throw new Error('Please login!!');
                    }
                    return PersonModel.findById(id).exec();
                }
            },
            images:{
                type: GraphQLList(SearchImageType),
                args:{
                    keywords: { type: GraphQLString }
                },
                resolve: (root, { keywords }, { user }, info) => {
                    if(!user){
                        throw new Error('Please login!!');
                    }
                    return search_images(keywords);

                }
            }
        }
    }),
    mutation: new GraphQLObjectType({
        name: "Mutation",
        fields: {
            signup: {
                type: AuthPayloadType,
                args: {
                    email: { type: GraphQLNonNull(GraphQLString) },
                    name: { type: GraphQLNonNull(GraphQLString) },
                    password: { type: GraphQLNonNull(GraphQLString) }
                },
                resolve: async (root, { name, email, password }, context, info) => {
                    const data = {
                        name,
                        email,
                        password: await bcrypt.hash(password, 10)
                    }
                    var person = new PersonModel(data);
                    const saved = await person.save();

                    // return json web token
                    return {
                        user: saved,
                        token: jsonwebtoken.sign(
                        { id: saved.id, email: saved.email },
                        process.env.JWT_KEY,
                        { expiresIn: '1y' }
                    )}
                }
            },
            login: {
                type: AuthPayloadType,
                args: {
                    email: { type: GraphQLNonNull(GraphQLString) },
                    password: { type: GraphQLNonNull(GraphQLString) }
                },
                resolve: async (root, {email, password}, context, info) => {
                    var user = await PersonModel.findOne({email}).exec();
                    if (!user) {
                        throw new Error('No such user found')
                    }
                
                    // 2
                    const valid = await bcrypt.compare(password, user.password)
                    if (!valid) {
                        throw new Error('Invalid password')
                    }
                    
                    const token = jsonwebtoken.sign(
                        { id: user.id, email: user.email },
                        process.env.JWT_KEY,
                        { expiresIn: '1y' }
                    );

                    // return json web token
                    return {
                        user,
                        token
                    }
                }
            }
        }
    })
});

module.exports = {
    schema
};