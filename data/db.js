const Mongoose = require("mongoose");

Mongoose.connect(process.env.DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

module.exports = Mongoose;