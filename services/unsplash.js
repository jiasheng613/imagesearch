const rp = require('request-promise');

const service_name = "Unsplash";

const format_display = (results) => {
    return results.map(({ id, urls, tags, title, links }) => {
        const { thumb } = urls;
        const { self } = links;
        return {
            image_ID: id,
            thumbnails: thumb,
            preview: self,
            title: title || null,
            source : service_name,
            tags: tags.map(t => t.title),
        }
    })
}

module.exports = {
    request:( keyword ) => {
        const options = {
            method: 'GET',
            uri: 'https://api.unsplash.com/search/photos/',
            qs: {
                query: keyword.split(' ').join('+')
            },
            headers: {
                'Authorization': `Client-ID ${process.env.UNSPLASH_KEY}`,
                'Content-Type': `application/json`,
            },
            json: true // Automatically parses the JSON string in the response
        };

        return rp(options)
        .then(({ results }) => format_display(results))
        .catch((err) => {
            return [];
        });
    }
}