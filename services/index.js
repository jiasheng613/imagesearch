const glob = require( 'glob' );

let libs = glob.sync( './*.js',{ignore:["./index.js"], cwd: __dirname} ).map( function( file ) {
    const name = file.replace('.js','');
    return name;
});
// console.log(libs)
// Promise.all(libs.map(v => require(v).request('dogs'))).then(console.log);
// require('./unsplash').request('dogs').then(console.log).catch(console.error)
module.exports = {
    search_images : (keywords) => {
        return Promise.all(libs.map(v => require(v).request(keywords))).then(arr => arr.flat());
    }
}