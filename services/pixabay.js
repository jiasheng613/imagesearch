const rp = require('request-promise');

const service_name = "Pixabay";

const format_display = (results) => {
    return results.map(({ id, tags, previewURL, largeImageURL }) => {
        return {
            image_ID: id,
            thumbnails: previewURL,
            preview: largeImageURL,
            title: null,
            source : service_name,
            tags: tags.split(', '),
        }
    })
}

module.exports = {
    request:( keyword ) => {
        const options = {
            method: 'GET',
            uri: 'https://pixabay.com/api/',
            qs: {
                q: keyword.split(' ').join('+'),
                key: process.env.PIXABAY_KEY
            },
            headers: {
                'Content-Type': `application/json`,
            },
            json: true // Automatically parses the JSON string in the response
        };

        return rp(options)
        .then(({ hits }) => format_display(hits))
        .catch((err) => {
            return [];
        });
    }
}