###build the container
```docker compose build```

###run the container
```docker compose up -d```

###stop the container
```docker compose down```

###load test
```
npm i
npm i -g artillery
artillery run artillery.yml
```