require('dotenv').config();
const Express = require("express");
const ExpressGraphQL = require("express-graphql");
const jwt = require('express-jwt');
const { schema } = require("./data/schema");


var app = Express();

// auth middleware
const auth = jwt({
    secret: process.env.JWT_KEY,
    credentialsRequired: false
});

app.use("/graphql",auth, ExpressGraphQL((req,res) => ({
    schema: schema,
    graphiql: true,
    context:{
       user: req.user
    }
})));

app.listen(3000, () => {
    console.log("Listening at :3000...");
});