const EasyGraphQLLoadTester  = require('easygraphql-load-tester');
const { schema } = require("./data/schema");

const args = {
  user: {
    id: '5dcfed9fc2adb70013358e75'
  },
  images: {
    keywords: '100 plus'
  },
  signup:{
    email: 'qwe@qwe.cc',
    name: 'qwe@qwe.cc',
    password: 'qwe@qwe.cc',
  },
  login:{
    email: 'qwe@qwe.cc',
    name: 'qwe@qwe.cc',
    password: 'qwe@qwe.cc',
  }
}
const loadTester = new EasyGraphQLLoadTester (schema,args);


// console.log(loadTester)
const testCases = loadTester.artillery()
// console.log(testCases)
module.exports = {
  testCases
}